__title__ = 'Django DigitalStoreMesh Authentication'
__version__ = '1.0.33'
__author__ = 'DigitalStoreMesh Co.,Ltd'
__license__ = 'MIT'

VERSION = __version__

import dsmauth.backend